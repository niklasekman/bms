
/** 
 *  @file   BMS.cpp
 *  @brief  Battery management library for Arduino 

With inspiration from

https://www.instructables.com/1S-6S-Battery-Voltage-Monitor-ROS/

https://arduino.stackexchange.com/questions/30205/returning-an-int-array-from-a-function

https://forum.arduino.cc/t/static-const-array-as-class-member/218337/6

https://forum.arduino.cc/t/define-a-constant-static-array-for-a-class/629621/4

https://stackoverflow.com/questions/37074763/arduino-class-array-member/50924520

 *  @author Niklas Ekman 
 *  @date   2021-11-16 
 * 
 */

#include <Arduino.h>
#include "BMS.h"

const double BMS::_cellConst[MAX_CELLS] = {1.000,1.0957,0.8990,1.0278,0.9467,1.1000,0.9429,0.9787,0.9407,0.9235,1.0000,0.9167};

BMS::BMS(int *cellPins, byte numCells) {
  _numCells = numCells;
  _cellPins = cellPins;
}



batteryHealth BMS::getHealth() {
  return GOOD;
}

double *BMS::getVoltages(double *cellVoltages){
  for(int i = 0; i < _numCells; i++) {
    cellVoltages[i] = getVoltage(i);
  }
  return cellVoltages;
}

double BMS::getVoltage(int cell) {
  double cellVoltage = analogRead(_cellPins[cell]) * K * _cellConst[cell];
  return cellVoltage;
}
