#include "BMS.h"
int pins[] = {4,5};
BMS bms = BMS(pins, 2);
double cellVoltages[2];

void setup() {

  Serial.begin(9600);
}

void loop() {

  bms.getVoltages(cellVoltages);
  Serial.print(cellVoltages[0]);
  Serial.print('\t');
  Serial.println(cellVoltages[1]);
}
