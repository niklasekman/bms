#include <Arduino.h>

#define K 0.00472199
#define MAX_CELLS 12

enum batteryHealth {
  UNDERVOLTAGE,
  OVERVOLTAGE,
  UNBALANCED,
  GOOD
};

class BMS {
  private:
   int *_cellPins;
   byte _numCells;
   static const double _cellConst[MAX_CELLS];
   double getVoltage(int cell);
   
  public:
    BMS(int *cellPins, byte numCells);
    
    batteryHealth getHealth();
    double *getVoltages(double *cellVoltages);
};
